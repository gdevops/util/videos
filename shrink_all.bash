﻿#!/usr/bin/env bash

for filename in *.mp4; do
    input=${filename}
    name=${filename%.*}
    suffixe=${filename##*.}
    output="hd480/"${name}_hd480.${suffixe}
    echo "Input: ${input}"
    echo "File Name: ${name}"
    echo "File Extension: ${suffixe}"
    echo "output: ${output}"

    ffmpeg -i ${input} -c:v libx264 -s hd480 -crf 22 -c:a aac -b:a 160k -vf "scale=iw*sar:ih,setsar=1" ${output}

done
